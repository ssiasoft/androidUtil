package ir.drax.expandable;

public interface Interpolate {
    public void interpolate(float interpolate);
}
